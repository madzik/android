package com.zwierzat.adopcja.aplicationanimals;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button zaloguj;
    Button zarejestrujSie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        zarejestrujSie =(Button) findViewById(R.id.zarejestrujSie);
        zaloguj = (Button) findViewById(R.id.zalogujSie);
        zaloguj.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ZalogujSie();
            }
        });
        zarejestrujSie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ZarejestrujSie();
            }
        });

    }

    public void ZalogujSie() {
        Toast.makeText(MainActivity.this, "otwieranie", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this,ZalogujSie.class);
        startActivity(intent);
    }
    public void ZarejestrujSie(){
        Intent intent = new Intent(MainActivity.this,ZarejestrujSie.class);
        startActivity(intent);
    }
}
