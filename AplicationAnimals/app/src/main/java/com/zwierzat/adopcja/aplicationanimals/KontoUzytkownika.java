package com.zwierzat.adopcja.aplicationanimals;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Magda on 2016-07-07.
 */
public class KontoUzytkownika extends AppCompatActivity {

    ImageView imageView;
    Button mButton;
    Button RecyclerButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konto_uzytkownika);
        imageView = (ImageView) findViewById(R.id.pets);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pets();
            }
        });
        mButton = (Button) findViewById(R.id.RecyclerView);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView();
            }
        });

    }

    public void pets(){
        String url = "http://www.fokus.tv/news/opieka-nad-zwierzetami-domowymi-co-robic-by-zachowac-higiene-i-porzadek-w-domu-pelnym-zwierzat/3204";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
    public void recyclerView(){
        Toast.makeText(KontoUzytkownika.this, "wchodzisz", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(KontoUzytkownika.this, Recycler.class);
        startActivity(intent);

    }
}

