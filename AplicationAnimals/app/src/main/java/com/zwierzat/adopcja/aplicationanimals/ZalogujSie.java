package com.zwierzat.adopcja.aplicationanimals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Magda on 2016-07-07.
 */
public class ZalogujSie extends AppCompatActivity {

    Button zalogujsie;
    EditText myEditText;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_zaloguj_sie);
      zalogujsie = (Button) findViewById(R.id.zalogujSie);
      zalogujsie.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              ZalogujSie();
          }

      });
      myEditText = (EditText) findViewById(R.id.Login);
      String strUserName = myEditText.getText().toString();

      if (TextUtils.isEmpty(strUserName)) {
          myEditText.setError("Wpisz dane");
          return;

      }
  }



      public void ZalogujSie() {
          Toast.makeText(ZalogujSie.this, "wchodzisz", Toast.LENGTH_LONG).show();
          Intent intent = new Intent(ZalogujSie.this, KontoUzytkownika.class);
          startActivity(intent);
      }
}


