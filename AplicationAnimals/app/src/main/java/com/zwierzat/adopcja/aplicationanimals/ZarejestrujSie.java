package com.zwierzat.adopcja.aplicationanimals;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Magda on 2016-07-07.
 */
public class ZarejestrujSie extends AppCompatActivity {


    public EditText naszTekst;
    public Button wyslij;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zarejestruj_sie);
        wyslij = (Button) findViewById(R.id.wyslij);
        wyslij.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Wyslij();
            }
        });
    }
    public void Wyslij(){
        Toast.makeText(ZarejestrujSie.this, "Wysłano",Toast.LENGTH_LONG).show();

    }
}
