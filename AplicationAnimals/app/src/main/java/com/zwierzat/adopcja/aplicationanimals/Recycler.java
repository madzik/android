package com.zwierzat.adopcja.aplicationanimals;

import android.app.Activity;
import android.icu.text.UnicodeSetSpanner;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Magda on 2016-07-13.
 */
public class Recycler extends Activity {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        mRecyclerView = (RecyclerView) findViewById(R.id.listView);
        mRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lista();
            }
        });
    }

    public void lista() {
        Toast.makeText(Recycler.this, "Recycler", Toast.LENGTH_SHORT).show();

    }
}